import XCTest
@testable import ControlSDKExt
@testable import ControlSDK

class ControlServiceTest: XCTestCase {
    
    let prod = ControlConfiguration(
        url: URL(string: "https://api.one.accedo.tv")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK"
    )
    
    let mock = ControlConfiguration(
        url: URL(string: "https://private-anon-5362822e2d-appgrid.apiary-mock.com")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK",
        validateSessionDate: false
    )
   
    func testAllEntries() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "All Entries")
        service.allEntries(params: nil, recursive: [], { res in
            print("RES: \(res)")
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 100.0)
    }
    
    func testEntryRecursive() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "App Status")
        service.entryById(id: "575ac9cfb85b9f01f9b26c04", params: nil, recursive: ["items", "page", "containers"] , { res in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 100.0)
    }
    
    
    func testEntryRecursive2() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "App Status")
        service.entryById(id: "575aca40b85b9f01f9b26c12", params: nil, recursive: ["items", "page", "containers"] , { res in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 100.0)
    }
    
    
    func testAssets2() {
        let service = ControlService(configuration: mock, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        
        service.assets(keys: ["bug_report", "offerings"], { res in
            print("RES: \(res)")
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    
}
