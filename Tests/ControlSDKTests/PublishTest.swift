import XCTest
@testable import ControlSDK

class PublishTest: XCTestCase {
    
    let prod = ControlConfiguration(
        url: URL(string: "https://api.one.accedo.tv")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK"
    )
    
    func testAllEntries() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Create session")
        service.allEntries(params: nil) { res in
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testEntriesByIds() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Create session")
        service.entriesByIds(ids: ["575acfa4b85b9f01f9b279d4","575aac106d3e8901e8bac912"], params: nil, { res in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    
    func testEntryByIdEntries() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Create session")
        service.entryById(id: "575aa9c06d3e8901e8ba73af", params: nil, { res in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testEntryByAlias() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Create session")
        service.entryByAlias(alias: "shows", params: nil) { res in
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }

    func testEntry() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        service.entryById(id: "575aa9c06d3e8901e8ba73af", params: nil, { res in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testEntry2() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        let expectation1 = XCTestExpectation(description: "Metadata 2")
        service.entryById(id: "575aa9c06d3e8901e8ba73af", params: nil, { res in
            expectation.fulfill()
            Thread.sleep(forTimeInterval: 3.0)
            service.entryById(id: "575aa9c06d3e8901e8ba73af", params: nil, { res in
                expectation1.fulfill()
            })
        })
        wait(for: [expectation, expectation1], timeout: 10.0)
    }
    
    
    func testAllEntriesEntry() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        service.allEntries(params: nil, { res in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
}

