import XCTest
@testable import ControlSDK

class UserDataTest: XCTestCase {
    
    let prod = ControlConfiguration(
        url: URL(string: "https://api.one.accedo.tv")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK"
    )
    
    struct TestUserData: Codable {
        var test: String = "What"
        var name: String = "Jane1"
    }
    
    func testGetUserData() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "GetUserData")
        
        service.getUserData(userId: "a_unique_user_identifier", { res in
            print("RES: \(res)")
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testGetApplicationUserData() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "GetApplicationUserData")
        service.getApplicationUserData(userId: "a_unique_user_identifier", { res in
            print("RES: \(res)")
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testGetUserDataKey() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "GetUserData")
        
        service.getUserData(userId: "a_unique_user_identifier", key: "test", { res in
            print("RES: \(res)")
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSetApplicationUserData() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "SetApplicationUserData")
        do {
            let data = try JSONEncoder().encode(TestUserData())
            service.setApplicationUserData(userId: "a_unique_user_identifier", data: data) { res in
                switch res {
                case .none:
                    break
                case .some(let err):
                    XCTFail(err.localizedDescription)
                    break
                }
                expectation.fulfill()
            }
        } catch let e {
            XCTFail(e.localizedDescription)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSetUserData() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "SetUserData")
        do {
            let data = try JSONEncoder().encode(TestUserData())
            service.setUserData(userId: "a_unique_user_identifier", data: data) { res in
                switch res {
                case .none:
                    break
                case .some(let err):
                    XCTFail(err.localizedDescription)
                    break
                }
                expectation.fulfill()
            }
        } catch let e {
            XCTFail(e.localizedDescription)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSetUserDataKey() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "SetUserData")
        service.setUserData(userId: "a_unique_user_identifier", key: "test", data: "John", { res in
            switch res {
            case .none:
                break
            case .some(let err):
                XCTFail(err.localizedDescription)
                break
            }
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
}

