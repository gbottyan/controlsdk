import XCTest
@testable import ControlSDK

class ControlTest: XCTestCase {
    
    let prod = ControlConfiguration(
        url: URL(string: "https://api.one.accedo.tv")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK"
    )
    
    let mock = ControlConfiguration(
        url: URL(string: "https://private-anon-5362822e2d-appgrid.apiary-mock.com")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK",
        validateSessionDate: false
    )
    
    

    
    func testMetadata() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        
        service.metadata( { res in
            expectation.fulfill()
            
        })
        
        
        
        wait(for: [expectation], timeout: 10.0)
    }
   
    func testMetadataSerial() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        let expectation1 = XCTestExpectation(description: "Metadata 2")
        service.metadata( { res in
            expectation.fulfill()
            service.metadata( { res in
                expectation1.fulfill()
            })
        })
        wait(for: [expectation, expectation1], timeout: 10.0)
    }
    
    func testAssets() {
        let service = ControlService(configuration: mock, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Metadata 1")
        
        service.assets({ res in
            print("RES: \(res)")
            expectation.fulfill()
        })
    
        wait(for: [expectation], timeout: 10.0)
    }
}

