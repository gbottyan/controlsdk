import XCTest
@testable import ControlSDK

class ControlServiceTest: XCTestCase {
    
    let prod = ControlConfiguration(
        url: URL(string: "https://api.one.accedo.tv")!,
        key: "53fb8fcbe4b077cb2acaa3a6",
        uuid: "ControlSDK"
    )
    
    func testAppStatus() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "App Status")
        service.applicationStatus { res in
            switch res {
            case let .success(status):
                XCTAssertTrue(status.status == .active)
                break
            case let .failure(err):
                XCTAssertTrue(false)
                break
            }
            expectation.fulfill()
        }
    }
    
    func testSession() {
        
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Create session")
        
        
        
        service.applicationStatus({ res in
            switch res {
            case .success:
                XCTAssertNotNil(service.sessionKey, "Session key exists.")
                break
            case .failure:
                break
            }
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSessionSerial() {
        let service = ControlService(configuration: prod, cache: DefaultControlCache())
        let expectation = XCTestExpectation(description: "Status 1")
        let expectation1 = XCTestExpectation(description: "Status 2")
        
        service.applicationStatus({ res in
            switch res {
            case let .success(status):
                XCTAssertNotNil(service.sessionKey, "Session key exists.")
                break
            case let .failure(err):
                break
            }
            expectation.fulfill()
        })
        
        service.applicationStatus({ res in
            switch res {
            case let .success(status):
                XCTAssertNotNil(service.sessionKey, "Session key exists.")
                break
            case let .failure(err):
                break
            }
            expectation1.fulfill()
        })
        wait(for: [expectation, expectation1], timeout: 10.0)
    }
}

