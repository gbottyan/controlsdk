// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(name: "ControlSDK",
                      platforms: [.tvOS(.v10),.iOS(.v10),],
                      products: [
                        .library(name: "ControlSDK", targets: ["ControlSDK"]),
                        .library(name: "ControlSDKExt", targets: ["ControlSDKExt"])
                      ],
                      dependencies: [
                        .package(
                            name: "Promises", url: "https://github.com/google/promises.git", from: "2.0.0"
                        )
                      ],
                      targets: [
                        .target(name: "ControlSDK", dependencies: []),
                        .target(name: "ControlSDKExt", dependencies: ["ControlSDK","Promises"]),
                        .testTarget(name: "ControlSDKTests", dependencies: ["ControlSDK"]),
                        .testTarget(name: "ControlSDKExtTests", dependencies: ["ControlSDKExt"]),
                      ])
