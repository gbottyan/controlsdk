Pod::Spec.new do |s|
    s.name         = "ControlSDK"
    s.version      = "0.0.1"
    s.summary      = "Control SDK library"
    s.description  = "Control SDK library"
    s.homepage     = "https://appgrid.docs.apiary.io/"
    s.license      = "Copyright © 2022 Accedo Broadband Inc. All rights reserved."
    s.authors      = "Accedo Broadband Inc."
	s.swift_version = '5.0'
    s.platform     = :ios
    s.platform     = :tvos

    s.ios.deployment_target  = "10.2"
    s.tvos.deployment_target = "10.1"

    s.source       = { :path => '.' }
    s.requires_arc = true

    s.module_name = 'ControlSDK'
  	s.default_subspec = 'Core'
	s.exclude_files = "**/Pods/**/*.{h,m,swift}"

  	s.subspec 'Core' do |cs|
    	cs.source_files        = "Sources/ControlSDK/**/*.{h,m,swift}"
		cs.public_header_files = "Sources/ControlSDK/**/*.h"
	end
end
