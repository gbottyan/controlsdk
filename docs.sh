PACKAGE="ControlSDK"
xcodebuild docbuild -scheme $PACKAGE  -destination 'platform=iOS Simulator,name=iPhone 12'  -derivedDataPath ./tmp
find ./tmp -type d -name '*.doccarchive' | grep $PACKAGE | xargs -I{} cp -R {} .
rm -rf ./tmp

#sourcedocs generate -- -scheme $PACKAGE -destination 'platform=iOS Simulator,name=iPhone 12'
