//
//  ControlProfile.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 23..
//

import Foundation

public struct ControlProfile: Decodable {
    public var profileId: String
    public var profileName: String
    public var profileDescription: String
    public var profileLastModified: String
    public var abTestCycleId: String
    public var abTestCycleName: String
}
