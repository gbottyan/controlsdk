//
//  ControlRequest.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

public struct ControlRequest {
    
    public enum RequestMethod: String {
        case get = "GET"
        case put = "PUT"
        case post = "POST"
        case delete = "DELETE"
    }
    
    public enum RequestPayload {
        case data(Data)
        case json([String: Any])
        case string(String)
        public var body: Data? {
            switch self {
            case let .data(data): return data
            case let .json(json): return try? JSONSerialization.data(withJSONObject: json, options: .fragmentsAllowed)
            case let .string(str): return str.data(using: .utf8)
            }
        }
    }
    var url: URL?
    var queryParameters: [String: String] = [:]
    var headerParameters: [String: String] = [:]
    var method: RequestMethod = .get
    var path: String?
    var payload: RequestPayload?
    var configuration: ControlConfiguration
    
    public init(configuration: ControlConfiguration, url: URL? = nil) {
        self.configuration = configuration
        self.url = url
    }
    
    public func adding(path: String) -> ControlRequest {
        var builder = self
        builder.path = path
        return builder
    }
    
    public func addingGID() -> ControlRequest {
        if let gid = configuration.gid {
            return appending(queryParameters: ["gid": gid])
        } else {
            return self
        }
    }
    
    public func addingUUIDHeader() -> ControlRequest {
        return appending(headerParameters: ["X-Uuid": configuration.uuid])
    }
    
    public func addingSessionHeader(key: String) -> ControlRequest {
        return appending(headerParameters: ["X-Session": key])
    }
    
    public func addingAppkeyHeader() -> ControlRequest {
        return appending(headerParameters: ["X-Application-Key": configuration.key])
    }
    
    public func addingCC() -> ControlRequest {
        if configuration.cc.isEmpty {
            return self
        } else {
            return appending(queryParameters: ["cc": configuration.cc.map({ (s1, s2) in return "\(s1):\(s2)" }).joined(separator: ",")])
        }
    }

    public func adding(method: RequestMethod) -> ControlRequest {
        var builder = self
        builder.method = method
        return builder
    }

    public func adding(queryParameters: [String: String]) -> ControlRequest {
        var builder = self
        builder.queryParameters = queryParameters
        return builder
    }

    public func adding(headerParameters: [String: String]) -> ControlRequest {
        var builder = self
        builder.headerParameters = headerParameters
        return builder
    }

    public func appending(headerParameters: [String: String]) -> ControlRequest {
        var builder = self
        var all: [String: String] = [:]

        for (k, v) in builder.headerParameters {
            all[k] = v
        }

        for (k, v) in headerParameters {
            all[k] = v
        }

        builder.headerParameters = all
        return builder
    }
        
    public func appending(queryParameters: [String: String]) -> ControlRequest {
        var builder = self
        var all: [String: String] = [:]

        for (k, v) in builder.queryParameters {
            all[k] = v
        }

        for (k, v) in queryParameters {
            all[k] = v
        }

        builder.queryParameters = all
        return builder
    }

    public func adding(payload: RequestPayload) -> ControlRequest {
        var builder = self
        builder.payload = payload
        return builder
    }
}

extension ControlRequest: CustomStringConvertible {
    public var description: String {
        
        var fullUrl = configuration.url
        
        if let url = url {
            fullUrl = url
        }
        
        if let path = path {
            fullUrl.append(path: path)
        }
        fullUrl.append(queryItems: queryParameters)
        var header = ""
        for (k, v) in headerParameters {
            header = header + "\t\(k) : \(v)\n"
        }
        var body = ""
        if let payload = payload {
            switch payload {
            case .data(let data):
                body = String(data: data, encoding: .utf8)!
            case .json(let dictionary):
                body = dictionary.description
            case .string(let str):
                body = str
            }
        }
        
        return "\(method) \(fullUrl)\n\(header)\n\(body)==\n"
        
    }
}

extension ControlRequest {
    public func adding(params: ControlParams?) -> ControlRequest {
        if let params = params {
            return appending(queryParameters: params.dict())
        } else {
            return self
        }
    }
}

extension ControlRequest {
    func urlRequest() -> URLRequest {
        
        
        var fullUrl = configuration.url
        if let url = url {
            fullUrl = url
        }
        
        if let path = path {
            fullUrl.append(path: path)
        }
        fullUrl.append(queryItems: queryParameters)
        var urlrequest = URLRequest(url: fullUrl)
        urlrequest.httpMethod = method.rawValue
        urlrequest.allHTTPHeaderFields = headerParameters
        urlrequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        //urlrequest.cachePolicy = .reloadRevalidatingCacheData
        if let payload = payload {
            urlrequest.httpBody = payload.body
        }
        return urlrequest
    }
}
