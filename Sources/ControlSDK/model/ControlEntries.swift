//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 27..
//

import Foundation

public struct ControlPagination: Decodable {
    var total: Int
    var size: Int
    var offset: Int
}

public struct ControlPage<T: Decodable>: Decodable {
    var entries: [T]
    var pagination: ControlPagination
}
