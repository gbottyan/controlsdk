//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 03. 02..
//

import Foundation

public enum ControlError: Error {
    case decodeError
    case idNotFound
    case entryNotaDictionary
    case entriesNotFound
    
}



