//
//  ControlStatus.swift
//
//
//  Created by Gabor Bottyan on 2021. 05. 02..
//

import Foundation

public enum Status: String, Decodable {
    case maintenance = "Maintenance"
    case unauthorized = "UNAUTHORIZED"
    case active = "Active"
}

public struct ControlStatus: Decodable {
    /// Status of the application
	public var status: Status
    
    /// Editorial message
    public var message: String?
}

extension ControlStatus: Hashable {}
