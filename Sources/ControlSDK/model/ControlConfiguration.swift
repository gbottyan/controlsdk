//
//  ControlConfiguration.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

public struct ControlConfiguration {
    
    /// Control URL
    public var url: URL
    
    /// The application key.
    public var key: String
    
    /// A unique user identifier.
    public var uuid: String
    
    /// An optional global identifier used for whitelisting.
    public var gid: String?
    
    /// An optional custom condition key-value pair used for profile allocation. This parameter is repeatable to support multiple custom conditions.
    public var cc: [(String,String)]
    
    public var validateSessionDate = true
    
    public init(url: URL, key: String, uuid: String, gid: String? = nil, cc: [(String,String)] = [], validateSessionDate: Bool = true) {
        self.url = url
        self.key = key
        self.uuid = uuid
        self.gid = gid
        self.cc = cc
        self.validateSessionDate = validateSessionDate
    }
}
