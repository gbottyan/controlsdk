//
//  ControlSession.swift
//
//
//  Created by Gabor Bottyan on 2021. 05. 01..
//

import Foundation

public struct ControlSession: Decodable {
    /// Session key returned from Control
	public var sessionKey: String
	var expiration: Date
    
    /// Returns if the session is valid
	public var isValid: Bool {
		expiration > Date()
	}
}
