//
//  ControlResponse.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

public struct ControlResponse {
    public var code: Int
    public var headers: [String: Any]
    public var data: Data?
}

extension ControlResponse: CustomStringConvertible {
    public var description: String {
        
        var header = ""
        for (k, v) in headers {
            header = header + "\t\(k) : \(v)\n"
        }
        return "\n\n\(code) \(data?.count ?? 0) bytes\n\(header)\n\(String(data: data ?? Data(), encoding: .utf8)!)\n==\n"
    }
}

