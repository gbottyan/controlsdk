//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation

public struct ControlLogEntry: Encodable {
    public var message: String
    public var logType: String
    public var timestamp: Int
    public var code: Int
    public var dim1: String
    public var dim2: String
    public var dim3: String
    public var dim4: String
}
