//
//  ControlParams.swift
//  
//
//  Created by Gabor Bottyan on 2/23/22.
//

import Foundation

public struct ControlParams {
    
    enum ControlParamNames: String, CaseIterable {
        case preview
        case at
        case locale
        case offset
        case size
        //case gid
        //case cc
    }
    /// Used to get Entry preview for a specific moment of time (past or future). Value is an ISO formatted date time string in UTC. Can not be used if preview is set to true.
    private var preview: Bool?
    /// Used to get Entry preview for a specific moment of time (past or future). Value is an ISO formatted date time string in UTC. Can not be used if preview is set to true.
    private var at: Date?
    /// The Locale Code. This parameter is used to specify which Locale you want the Entry to contain. Omitting this parameter returns the default Locale.
    private var locale: String?
    /// The offset in the pagination. Default is 0.
    private var offset: Int?
    /// The number of results per page. A number between 1 and 50, the default is 20.
    private var size: Int?
    /// An optional global identifier used for whitelisting.
    private var gid: String?
    /// An optional custom condition key-value pair used for profile allocation. This parameter is repeatable to support multiple custom conditions.
    public var cc: [(String,String)] = []
    
    let formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "UTC")
        return dateFormatter
    }()
    
    /// Public default constructor
    public init() {}
    
    /// Return unpublished entries
    /// - Parameter preview: Determines whether unpublished Entries should be returned. Additionally, any modifications to the Entry that are yet to be published will be returned.
    /// - Returns: Modified ControlParams value
    public func preview(_ preview: Bool) -> Self {
        var builder = self
        builder.preview = preview
        return builder
    }
    
    /// Used to get Entry preview for a specific moment of time (past or future).
    /// - Parameter at: Value is an ISO formatted date time. Can not be used if preview is set to true.
    /// - Returns: Modified ControlParams value
    public func at(_ at: Date) -> Self {
        var builder = self
        builder.at = at
        return builder
    }
    
    /// Filter returned entries based on locale
    /// - Parameter locale: The Locale Code. This parameter is used to specify which Locale you want the Entry to contain. Omitting this parameter returns the default Locale.
    /// - Returns: Modified ControlParams value
    public func locale(_ locale: String) -> Self {
        var builder = self
        builder.locale = locale
        return builder
    }
    
    /// The offset in the pagination. Default is 0.
    /// - Parameter offset: The offset in the pagination.
    /// - Returns: Modified ControlParams value
    public func offset(_ offset: Int) -> Self {
        var builder = self
        builder.offset = offset
        return builder
    }
    
    /// The number of results per page. A number between 1 and 50, the default is 20.
    /// - Parameter size: Page size
    /// - Returns: Modified ControlParams value
    public func size(_ size: Int) -> Self {
        var builder = self
        builder.size = size
        return builder
    }
    
    func dict() -> [String: String] {
        var dict = [String: String]()
        
        if let at = at {
            dict[ControlParamNames.preview.rawValue] = formatter.string(from: at)
        }
        
        if let preview = preview {
            dict[ControlParamNames.preview.rawValue] = preview ? "true" : "false"
        }
        
        if let locale = locale {
            dict[ControlParamNames.locale.rawValue] = locale
        }
        
        if let size = size {
            dict[ControlParamNames.size.rawValue] = "\(size)"
        }
        
        if let offset = offset {
            dict[ControlParamNames.offset.rawValue] = "\(offset)"
        }
        
        return dict
    }
}
