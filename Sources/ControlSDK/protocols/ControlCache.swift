//
//  ControlServiceCache.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

public protocol ControlCache {
    func write(key: String, data: ControlResponse)
    func lastValue(key: String) -> String?
    func read(key: String) -> ControlResponse?
}
