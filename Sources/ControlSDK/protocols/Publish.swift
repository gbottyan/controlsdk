//
//  Publish.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

/// A Content Entry is based on an Entry Type (defined in the Admin interface).
/// Entries contain a number of fields, which have generic or platform-specific values.
/// Date fields in responses are presented in UTC.
public protocol Publish: ControlSDK {
    
    /// Get a single Entry by requesting a single Entry ID.
    /// - Parameters:
    ///   - id: The ID of the Entry. Example: 57daa5e6613d6f0006d115a2.
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func entryById(id: String, params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Get a single Entry by requesting a single Entry Alias.
    /// - Parameters:
    ///   - alias: The alias of the Entry.
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func entryByAlias(alias: String, params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Get several Entries by passing in a list of Entry IDs as a request parameter.
    /// The entries in the response will be in the same order as the IDs sent in.
    /// The response is paginated with size and offset. (see: ControlParams )
    /// - Parameters:
    ///   - ids: List of Entry IDs.
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func entriesByIds(ids: [String], params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Get several Entries by passing in a list of Entry Aliases as a request parameter.
    /// The entries in the response will be in the same order as the IDs sent in.
    /// The query is paginated with offset and size. (see: ControlParams )
    /// - Parameters:
    ///   - aliases:List of Entry Aliases.
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func entriesByAliases(aliases: [String], params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Get a list of Entries by passing in one or several (comma-separated) Entry Type ID(s) as a request parameter.
    /// The entries in the list are ordered by most recently published first.
    /// If preview is set to true the entries will be ordered by last modified first.  (see: ControlParams )
    /// The query is paginated with offset and size.  (see: ControlParams )
    /// - Parameters:
    ///   - typeId: List of Entry Type IDs.
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func entriesByTypeId(typeIds: [String], params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Get a list of Entries by passing in an Entry Type Alias as a request parameter.
    /// The entries in the list are ordered by most recently published first.
    /// If preview is set to true the entries will be ordered by last modified first.  (see: ControlParams )
    /// The query is paginated with offset and size. (see: ControlParams )
    /// - Parameters:
    ///   - typeAlias: An Entry Type Alias.
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func entriesByTypeAlias(typeAlias: String, params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Get all Entries that are published on the Platform your Application is on, in a paginated list.
    /// The Entries in the list are ordered by most recently published first by default.
    /// If preview is set to true, the list will be ordered by the Entries last modified date.
    /// The response is paginated with size and offset. Note that the timestamps in the _meta object are returned in UTC.
    /// - Parameters:
    ///   - params: ControlParams structure to filter response
    ///   - completion: closure called with the result
    func allEntries(params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// This endpoint is used to get all available locales for the Organization.
    /// - Parameter completion: closure called with the result
    func allLocales(_ completion: @escaping (Result<Data, Error>) -> Void)
}

extension Publish {
    
    func entryById<T: Decodable>(id: String, params: ControlParams?, to: T.Type, _ completion: @escaping (Result<T, Error>) -> Void) {
        entryById(id: id, params: params, dataToTypeMapping(completion))
    }
    
    func entryByAlias<T: Decodable>(alias: String, params: ControlParams?, to: T.Type, _ completion: @escaping (Result<T, Error>) -> Void) {
        entryByAlias(alias: alias, params: params, dataToTypeMapping(completion))
    }
    
    func entriesByIds<T: Decodable>(ids: [String], params: ControlParams?, to: T.Type, _ completion: @escaping (Result<ControlPage<T>, Error>) -> Void) {
        entriesByIds(ids: ids, params: params, dataToTypeMapping(completion))
    }
    
    func entriesByAliases<T: Decodable>(aliases: [String], params: ControlParams?, to: T.Type, _ completion: @escaping (Result<ControlPage<T>, Error>) -> Void) {
        entriesByAliases(aliases: aliases, params: params, dataToTypeMapping(completion))
    }
    
    func entriesByTypeId<T: Decodable>(typeIds: [String], params: ControlParams?, to: T.Type, _ completion: @escaping (Result<ControlPage<T>, Error>) -> Void) {
        entriesByTypeId(typeIds: typeIds, params: params, dataToTypeMapping(completion))
    }
    
    func entriesByTypeAlias<T: Decodable>(typeAlias: String, params: ControlParams?,to: T.Type,  _ completion: @escaping (Result<ControlPage<T>, Error>) -> Void) {
        entriesByTypeAlias(typeAlias: typeAlias, params: params, dataToTypeMapping(completion))
    }
    
    func allEntries<T: Decodable>(params: ControlParams?, to: T.Type, _ completion: @escaping (Result<ControlPage<T>, Error>) -> Void) {
        allEntries(params: params, dataToTypeMapping(completion))
    }
}

