//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation

public protocol Insight: ControlSDK {
    /// A START event should be fired as soon as the application either starts or resumes (from having been in the background).
    /// - Parameter _completion: closure called with the result
    func applicationStart(_completion: @escaping (Error?) -> Void)
    
    /// A QUIT event should be fired as soon as the application is killed or when it is put into the background.
    /// - Parameters:
    ///   - retentionTimne: Control uses as the time spent by the user in the application. The retention time attributeIf not supplied, Control will automatically try to match the QUIT event with the most recent START event that is yet to be matched in the last 8 hours.
    ///   - _completion:  closure called with the result
    func applicationQuit(retentionTimne: Int, _completion: @escaping (Error?) -> Void)
}
