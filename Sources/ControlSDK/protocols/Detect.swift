//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation

/// Applications can send so called application logs to the Accedo One API in order to capture any kind of error happening within the application during runtime.The following log levels are supported: error, warn, info and debug.
public protocol Detect: ControlSDK {
    /// Returns log level for application, response contains one of the following values error, warn, info, debug or off if logging is disabled. If application tries to post an application log message with a higher verbosity that the one returned by this method - it will be discarded.
    /// - Parameter completion: closure called with the result
    func applicationLogLevel(_ completion: @escaping (Result<String, Error>) -> Void)
    /// Post a log message to Accedo One which could be used for error reporting and troubleshooting.
    /// - Parameters:
    ///   - message: Message
    ///   - completion: closure called with the result
    func log(message: ControlLogEntry, _ completion: @escaping (Error?) -> Void)
    /// Post several log messages in one batch to Accedo One which could later be used for, for example, reports and error detection.
    /// - Parameters:
    ///   - messages: list of messages
    ///   - completion: closure called with the result
    func log(messages: [ControlLogEntry], _ completion: @escaping (Error?) -> Void)
}

