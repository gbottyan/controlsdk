//
//  Control.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

/// Configurable parameters for applications can be placed into key-value pairs in Accedo One and accessed through the Metadata APIs.To provide flexible access to configuration, these APIs have support for wildcards and partial keys. For example, to select all keys starting with an 'a' you would request for a*.These API’s honor whitelists that a client might be matching
public protocol Control: ControlSDK {
    
    /// Get all available metadata entries based on active profile or whitelist override. This method does not provide any filtering parameters and could result in a large dataset being returned. If this is not desired you may want to use  metadata(key: , _ completion:  )
    /// - Parameter completion: closure called with the result
    func metadata(_ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// Applications with complex metadata structure often need only one key for a specific action. To avoid the overhead of transferring and parsing a large set of configuration values, this method could be used to get one single metadata entry by key. The key could be a full key name or end with a wildcard for flexible matching
    /// - Parameters:
    ///   - key: String key or wildcard (ex: a*)
    ///   - completion: closure called with the result
    func metadata(key: String, _ completion: @escaping (Result<Data, Error>) -> Void )
    
    /// Applications with complex metadata structure often need only one key for a specific action. To avoid the overhead of transferring and parsing a large set of configuration values, this method could be used to get one single metadata entry by key. The key could be a full key name or end with a wildcard for flexible matching
    /// - Parameters:
    ///   - keys: an array of String keys or wildcards
    ///   - completion: closure called with the result
    func metadata(keys: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    
    /// This method can be used to query for information about which profile your device and application is matching right now.When used together with the A/B testing feature of Accedo One, this call can be used to know which variant (i.e. Profile) was received, and the name and ID of the A/B test that was matched. Applications can then use this call to tag events sent to another analytics platform with the profileId and the abTestCycleId that the device received.
    /// - Parameter completion: closure called with the result
    func getProfile(_ completion: @escaping (Result<ControlProfile, Error>) -> Void)
    
    /// This method is used for listing assets available to the application. It returns a list of assets available in the format of key-url values.
    /// - Parameter completion: closure called with the result
    func assets(_ completion: @escaping (Result<[String: String], Error>) -> Void)
}

extension Control {
    
    func metadata<T: Decodable>(to: T.Type, _ completion: @escaping (Result<T, Error>) -> Void) {
        metadata(dataToTypeMapping(completion))
    }
    
    func metadata<T: Decodable>(key: String, to: T.Type, _ completion: @escaping (Result<T, Error>) -> Void ) {
        metadata(key: key, dataToTypeMapping(completion))
    }
    
    func metadata<T: Decodable>(keys: [String], to: T.Type, _ completion: @escaping (Result<T, Error>) -> Void) {
        metadata(keys: keys, dataToTypeMapping(completion))
    }
}
