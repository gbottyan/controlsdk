//
//  ControlSDK.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 27..
//

import Foundation

/// Control is a package to access Control API.
public protocol ControlSDK {}

extension ControlSDK {
    func dataToTypeMapping<T: Decodable>(_ completion: @escaping (Result<T, Error>) -> Void) -> (Result<Data, Error>) -> Void {
        return { res in
            switch res {
            case let .success(data):
                do {
                    completion(.success(try JSONDecoder().decode(T.self, from: data)))
                } catch let e {
                    completion(.failure(e))
                }
            case let .failure(err):
                completion(.failure(err))
            }
        }
    }
}
