//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation

public protocol UserData: ControlSDK {
    func getUserData(userId: String, _ completion: @escaping (Result<Data, Error>) -> Void)
    func setUserData(userId: String, data: Data, _ completion: @escaping (Error?) -> Void)
    func getUserData(userId: String, key: String,  _ completion: @escaping (Result<Data, Error>) -> Void)
    func setUserData(userId: String, key: String, data: String,  _ completion: @escaping (Error?) -> Void)
    func getApplicationUserData(userId: String, _ completion: @escaping (Result<Data, Error>) -> Void)
    func setApplicationUserData(userId: String, data: Data, _ completion: @escaping (Error?) -> Void)
    func getApplicationUserData(userId: String, key: String,  _ completion: @escaping (Result<Data, Error>) -> Void)
    func setApplicationUserData(userId: String, key: String, data: String,  _ completion: @escaping (Error?) -> Void)
}
