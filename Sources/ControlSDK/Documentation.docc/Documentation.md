# Getting started with ControlSDK

![Swift](https://img.shields.io/badge/Swift-v5.5-orange.svg?style=plastic)
![Unit Test](https://img.shields.io/badge/UnitTests-0%25-green.svg?style=plastic)

## Introduction

Control is a package to access Control API.

## Installation

### Swift Package Manager

```swift
.package(url: "https://github.com/Accedo-Products/accedo-control-sdk-ios.git", from: "0.0.1"),
.package(url: "https://github.com/Accedo-Products/accedo-control-sdk-ios.git", branch: "master"),
```

### Cocoapods

```ruby
pod 'ControlSDK', :git => 'https://github.com/Accedo-Products/accedo-control-sdk-ios.git'
```

## Basic Usage

```swift
import ControlSDK
/// Create control configuration
let configuration = ControlConfiguration(
    url: URL(string: "https://api.one.accedo.tv")!,
    key: "your api key",
    uuid: "your uuid")

/// Create service instance with configuration and cache handler
let service = ControlService(
    configuration: configuration, 
    cache: DefaultControlCache()
)

/// Call service ....
service.applicationStatus({ res in
    switch res {
    case .success:
        /// .....
    case .failure:
        /// ....
    }            
})
```

### How to query Control Metadata?

Configurable parameters for applications can be placed into key-value pairs in Accedo One and accessed through the Metadata APIs.

To provide flexible access to configuration, these APIs have support for wildcards and partial keys. For example, to select all keys starting with an 'a' you would request for `a*`.

These API’s honor whitelists that a client might be matching.

Metadata response structure depends on the value type configured in Admin UI.

```swift
import ControlSDK
/// Create control configuration
let configuration = ControlConfiguration(
    url: URL(string: "https://api.one.accedo.tv")!,
    key: "your api key",
    uuid: "your uuid")

/// Create service instance with configuration and cache handler
let service = ControlService(
    configuration: configuration, 
    cache: DefaultControlCache()
)

service.metadata( { res in
    switch res {
    case let .success(data):
        break
    case let .failure(e):
        break
    }
})

// In order to receive a Decodable entry as response:
service.metadata(to: MyType.self, { res in
    switch res {
    case let .success(data):
        // data with type MyType
        break
    case let .failure(e):
        break
    }
})

```

### How to query Assets

Assets are files uploaded and managed through Accedo One. Typical usage scenarios are uploading images, configuration files or other resources that might need to be swapped on-the-fly by the client. These APIs honor whitelists that the client might be matching.

```swift
import ControlSDK
/// Create control configuration
let configuration = ControlConfiguration(
    url: URL(string: "https://api.one.accedo.tv")!,
    key: "your api key",
    uuid: "your uuid")

/// Create service instance with configuration and cache handler
let service = ControlService(
    configuration: configuration, 
    cache: DefaultControlCache()
)

service.assets({ res in
    switch res {
    case let .success(data):
        /// Returns [String: String] as key: URL pairs
        break
    case let .failure(e):
        break
    }
})
```
In order to get a list of assets:

```swift
import ControlSDK
import ControlSDKExt

/// Create control configuration
let configuration = ControlConfiguration(
    url: URL(string: "https://api.one.accedo.tv")!,
    key: "your api key",
    uuid: "your uuid")

/// Create service instance with configuration and cache handler
let service = ControlService(
    configuration: configuration, 
    cache: DefaultControlCache()
)

service.assets(keys: ["assetkey1", "assetkey2"], { res in
    switch res {
    case let .success(data):
        /// Returns [String: Data] as key: Data pairs
        break
    case let .failure(e):
        break
    }
})
```

### How to query Entries

A Content Entry is based on an Entry Type (defined in the Admin interface). Entries contain a number of fields, which have generic or platform-specific values.

```swift
import ControlSDK
/// Create control configuration
let configuration = ControlConfiguration(
    url: URL(string: "https://api.one.accedo.tv")!,
    key: "your api key",
    uuid: "your uuid")

/// Create service instance with configuration and cache handler
let service = ControlService(
    configuration: configuration, 
    cache: DefaultControlCache()
)

service.allEntries(params: nil) { res in
    switch res {
    case let .success(data):
        /// Returns [String: Data] as key: Data pairs
        break
    case let .failure(e):
        break
    }
})
```

### How to query Entries recursively

```swift
import ControlSDK
import ControlSDKExt

/// Create control configuration
let configuration = ControlConfiguration(
    url: URL(string: "https://api.one.accedo.tv")!,
    key: "your api key",
    uuid: "your uuid")

/// Create service instance with configuration and cache handler
let service = ControlService(
    configuration: configuration, 
    cache: DefaultControlCache()
)

service.entryById(id: "575aca40b85b9f01f9b26c12", params: nil, recursive: ["key1", "key2"] , { res in
    switch res {
    case let .success(data):
        /// Returns [String: Data] as key: Data pairs
        break
    case let .failure(e):
        break          
})
```

