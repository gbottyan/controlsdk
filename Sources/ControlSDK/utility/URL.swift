//
//  URL.swift
//  
//
//  Created by Gabor Bottyan on 2/23/22.
//

import Foundation

///
/// Extension URL with utilities functions.
///
public extension URL {
    /// Add a path to current URL
    mutating func append(path: String) {
        self = appending(path: path)
    }

    /// Extend path to the url and return new URL instance.
    func appending(path: String) -> URL {
        guard var urlComponents = URLComponents(string: absoluteString) else {
            return absoluteURL
        }

        urlComponents.path += path.starts(with: "/") ? path : "/" + path

        return urlComponents.url ?? absoluteURL
    }

    /// add given query item to the current URL.
    mutating func append(queryItem: String, value: String?) {
        self = appending(queryItem: queryItem, value: value)
    }

    /// Add given query item and return a copy URL.
    func appending(queryItem: String, value: String?) -> URL {
        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        var queryItems: [URLQueryItem] = urlComponents.queryItems ?? []
        let queryItem = URLQueryItem(name: queryItem, value: value)

        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems

        return urlComponents.url!
    }

    /// add given query items to the current URL.
    mutating func append(queryItems: [String: String?]) {
        self = appending(queryItems: queryItems)
    }

    /// Add given query items and return a copy URL.
    func appending(queryItems: [String: String?]) -> URL {
        var url = self
        let sortedKeys = Array(queryItems.keys).sorted()
        sortedKeys.forEach {
            url = url.appending(queryItem: $0, value: queryItems[$0, default: nil])
        }
        return url
    }

    /// Initalise with a static String. No optional needed, will force unwrap.
    init(staticString: StaticString) {
        self.init(string: "\(staticString)")!
    }

    /// Returns list of query items for the URL, or an empty dictionary if none.
    var queryItems: [String: String] {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
              let queryItems = components.queryItems
        else {
            return [:]
        }
        return queryItems.reduce(into: [String: String]()) { result, item in
            result[item.name] = item.value
        }
    }
}
