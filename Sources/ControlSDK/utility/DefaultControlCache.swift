//
//  DefaultControlCache.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

class DefaultControlCache: ControlCache {
        
    struct CacheEntry {
        var date: Date
        var data: ControlResponse
    }
    
    var storage: [String: CacheEntry] = [:]
    
    func write(key: String, data: ControlResponse) {
        storage[key] = CacheEntry(date: Date(), data: data)
    }
    
    func lastValue(key: String) -> String? {
        return storage[key]?.data.headers["Last-Modified"] as? String
    }
    
    func read(key: String) -> ControlResponse? {
        return storage[key]?.data
    }
}
