//
//  ControlService.swift
//  
//
//  Created by Gabor Bottyan on 2/22/22.
//

import Foundation

public class ControlService: NSObject, URLSessionDelegate {
    
    class ControlServiceQueue {
        private var cacheQueue = DispatchQueue(label: "cache.queue", attributes: .concurrent)
        private var cacheActivity: [String: DispatchGroup] = [:]
        
        func loading(key: String) -> DispatchGroup {
            let group = DispatchGroup()
            self.cacheQueue.async(flags: .barrier) {
                self.cacheActivity[key] = group
            }
            return group
        }
        
        func release(key: String) {
            self.cacheQueue.async(flags: .barrier) {
                self.cacheActivity.removeValue(forKey: key)
            }
        }
        
        func isLoading(key: String) -> DispatchGroup? {
            var res: DispatchGroup?
            self.cacheQueue.sync {
                res = self.cacheActivity[key]
            }
            return res
        }
    }
    
    private let semaphore = DispatchSemaphore(value: 0)
    private let sessionQueue = DispatchQueue(label: "session")
    private let controlServiceQueue = ControlServiceQueue()
    
    private var cache: ControlCache
    private var session: ControlSession?
    public let configuration: ControlConfiguration
    
    var sessionKey: String? {
        return session?.sessionKey
    }
    
    var headerFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "US")
        return dateFormatter
    }()
    
    var formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }()
    
    lazy var sessionDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(self.formatter)
        return decoder
    }()
    
    lazy var sessionRequest = ControlRequest(configuration: configuration)
        .adding(path: "session")
        .addingUUIDHeader()
        .addingCC()
        .addingAppkeyHeader()
        .addingGID()
    
    public init(configuration: ControlConfiguration, cache: ControlCache) {
        self.configuration = configuration
        self.cache = cache
    }
    
    func execute(request: ControlRequest, completion: @escaping (Result<ControlResponse, Error>) -> Void) {
        
        print("\n\n====> \(request)")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(
            configuration: configuration,
            delegate: self,
            delegateQueue: OperationQueue.current)
        
        let task = session.dataTask(with: request.urlRequest()) { data, urlResponse, error in
            
            if let error = error {
                completion(.failure(error))
            }
            
            var headers: [String: Any] = [:]
            if let httpResponse = urlResponse as? HTTPURLResponse {
                let code = httpResponse.statusCode
                httpResponse.allHeaderFields.forEach {
                    headers["\($0)"] = $1
                }
                let response = ControlResponse(code: code, headers: headers, data: data)
                print("<=\(response)")
                completion(.success(response))
            } else {
                print("Request error: \(NSError(domain: "Failed", code: 408, userInfo: nil))")
                completion(.failure(NSError(domain: "Failed", code: 408, userInfo: nil)))
            }
        }
        
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    func session(completion: @escaping (Result<String, Error>) -> Void) {
        sessionQueue.async {
            
            if let session = self.session, (session.isValid  || !self.configuration.validateSessionDate) {
                let key = session.sessionKey
                completion(.success(key))
                self.semaphore.signal()
            } else {
                self.execute(request: self.sessionRequest, completion: { res in
                    switch res {
                    case let .success(data):
                        
                        if let data = data.data {
                            do  {
                                let session = try self.sessionDecoder.decode(ControlSession.self, from: data)
                                print("Session: \(session)")
                                if (session.isValid || !self.configuration.validateSessionDate){
                                    self.session = session
                                    let key = session.sessionKey
                                    completion(.success(key))
                                } else {
                                    print("Session invalid \n\n\n\n\n")
                                    completion(.failure(NSError(domain: "Session invalid", code: 0, userInfo: nil)))
                                }
                            } catch let e {
                                print("Session decode : \(e) \n\n\n\n\n")
                                completion(.failure(e))
                            }
                        } else {
                            print("NO SESSION DATA \n\n\n\n\n")
                            completion(.failure(NSError(domain: "No data", code: 0, userInfo: nil)))
                        }
                        self.semaphore.signal()
                    case let .failure(err):
                        completion(.failure(err))
                        self.semaphore.signal()
                    }
                })
            }
            
            //let now = Date().timeIntervalSince1970
            //print("WAITING FOR SERIAL REQUEST \(key)")
            _ = self.semaphore.wait(wallTimeout: .distantFuture)
            //print("WAITING FOR SERIAL REQUEST FINISHED \(key) \(Date().timeIntervalSince1970 - now)")
        }
    }
    
    func executeCached(request: ControlRequest, completion: @escaping (Result<ControlResponse, Error>) -> Void) {
        var request = request
        let key = request.urlRequest().url?.description ?? ""
        DispatchQueue.global().async {
            if let lock = self.controlServiceQueue.isLoading(key: key) {
                lock.notify(queue: DispatchQueue.global(), execute: {
                    if let data = self.cache.read(key: key) {
                        print("###################")
                        completion(.success(data))
                    } else {
                        completion(.failure(NSError(domain: "Cache error", code: 0, userInfo: nil)))
                    }
                })
            } else {
                let newLock = self.controlServiceQueue.loading(key: key)
                newLock.enter()
                if let last = self.cache.lastValue(key: key) {
                    request = request.appending(headerParameters: ["If-Modified-Since": last])
                }
                self.execute(request: request, completion: { res in
                    switch res {
                    case let .success(response):
                        if response.code == 304 {
                            print("\n\n\n****************************\n\n\n")
                            if let data = self.cache.read(key: key) {
                                completion(.success(data))
                            } else {
                                completion(.failure(NSError(domain: "Cache error", code: 0, userInfo: nil)))
                            }
                        } else {
                            self.cache.write(key: key, data: response)
                            completion(.success(response))
                        }
                        
                        
                    case let .failure(err):
                        completion(.failure(err))
                    }
                    newLock.leave()
                    self.controlServiceQueue.release(key: key)
                })
            }
        }
    }
    
    func executeWithSession(request: ControlRequest, _ completion: @escaping (Result<ControlResponse, Error>) -> Void) {
        session(completion: { res in
            switch res {
            case let .success(sessionkey):
                self.execute(request: request.addingSessionHeader(key: sessionkey) ,completion: { res in
                    switch res {
                    case let .success(response):
                        completion(.success(response))
                    case let .failure(err):
                        completion(.failure(err))
                    }
                })
                
            case let .failure(err):
                completion(.failure(err))
            }
        })
    }
    
    public func executeCachedWithSession(request: ControlRequest, _ completion: @escaping (Result<ControlResponse, Error>) -> Void) {
        session(completion: { res in
            switch res {
            case let .success(sessionkey):
                self.executeCached(request: request.addingSessionHeader(key: sessionkey) ,completion: { res in
                    switch res {
                    case let .success(response):
                        completion(.success(response))
                    case let .failure(err):
                        completion(.failure(err))
                    }
                })
            case let .failure(err):
                completion(.failure(err))
            }
        })
    }
    
    func responseToDataMapping(_ completion: @escaping (Result<Data, Error>) -> Void) -> (Result<ControlResponse, Error>) -> Void {
        return { res in
            switch res {
            case let .success(response):
                if let data = response.data {
                    completion(.success(data))
                } else {
                    completion(.failure(NSError(domain: "No data", code: 408, userInfo: nil)))
                }
            case let .failure(err):
                completion(.failure(err))
            }
        }
    }
    
    func responseToTypeMapping<T: Decodable>(_ completion: @escaping (Result<T, Error>) -> Void) -> (Result<ControlResponse,Error>) -> Void {
        return { res in
            switch res {
            case let .success(response):
                if let data = response.data {
                    do {
                        completion(.success(try JSONDecoder().decode(T.self, from: data)))
                    } catch let e {
                        completion(.failure(e))
                    }
                } else {
                    completion(.failure(NSError(domain: "No data", code: 408, userInfo: nil)))
                }
            case let .failure(err):
                completion(.failure(err))
            }
        }
    }
}

