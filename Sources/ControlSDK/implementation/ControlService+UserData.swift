//
//  ControlService+UserData.swift
//  
//
//  Created by Gabor Bottyan on 2022. 03. 03..
//

import Foundation

extension ControlService: UserData {
    
    /// Get all of the user’s data stored uniquely for this application. Data is returned in the form of a JSON object with Key-Value pairs.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - completion: closure called with the result
    public func getUserData(userId: String, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "user/\(userId)")
                            .addingCC()
                            .addingGID(), responseToDataMapping(completion))
    }
    
    /// Get user’s data for a single key stored uniquely for this application. In contrast to Get User Data in the Get/Set User Data in Application Scope section
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - key: the scecified key
    ///   - completion: closure called with the result
    public func getUserData(userId: String, key: String, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "user/\(userId)/name")
                            .appending(queryParameters: ["key": key, "json": "true"])
                            .addingCC()
                            .addingGID(), responseToDataMapping(completion))
    }
    
    /// Get all of a user’s data stored on the application group level and visible for all application of the group this application belongs to.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - completion: closure called with the result
    public func getApplicationUserData(userId: String, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "group/\(userId)")
                            .addingCC()
                            .addingGID(), responseToDataMapping(completion))
    }
    
    /// Get a user’s data for a single key stored on the application group level and visible for all application of the group this application belongs to.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - key: the scecified key
    ///   - completion: closure called with the result
    public func getApplicationUserData(userId: String, key: String, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "group/\(userId)/name")
                            .appending(headerParameters: ["Content-Type":"application/json; charset=utf-8"])
                            .addingCC()
                            .addingGID(), responseToDataMapping(completion))
    }
    
    /// Set and overwrite any existing data for the user. This data is stored in application scope and is only available to this application.Caution: This method does not simply replace conflicting keys. All user data will be replaced by the information provided in the request - if the request does not contain a key - data saved for that key will be lost.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - data: data to store
    ///   - completion: closure called with the result
    public func setUserData(userId: String, data: Data, _ completion: @escaping (Error?) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(method: .post)
                            .adding(payload: .data(data))
                            .appending(headerParameters: ["Content-Type":"application/json; charset=utf-8"])
                            .adding(path: "user/\(userId)")
                            .addingCC()
                            .addingGID(), { res in
            switch res {
            case .success(_):
                completion(nil)
            case .failure(let err):
                completion(err)
            }
        })
    }
    
    /// Set data for the user for a specified key. This data is stored in application scope and is only available to this application. If the given key already exists, it will be overwritten.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - key: the scecified key
    ///   - data: data to store
    ///   - completion: closure called with the result
    public func setUserData(userId: String, key: String, data: String, _ completion: @escaping (Error?) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(method: .post)
                            .adding(payload: .json(["\(key)": "\(data)"]))
                            .appending(queryParameters: ["key": key])
                            .appending(headerParameters: ["Content-Type":"application/json; charset=utf-8"])
                            .adding(path: "user/\(userId)")
                            .addingCC()
                            .addingGID(), { res in
            switch res {
            case .success(_):
                completion(nil)
            case .failure(let err):
                completion(err)
            }
        })
    }
    
    /// Set and overwrite any existing data for the user. This data is stored in application group scope and is available to all applications belonging to the same application group.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - data: the scecified key
    ///   - completion: closure called with the result
    public func setApplicationUserData(userId: String, data: Data, _ completion: @escaping (Error?) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(method: .post)
                            .adding(payload: .data(data))
                            .appending(headerParameters: ["Content-Type":"application/json; charset=utf-8"])
                            .adding(path: "group/\(userId)")
                            .addingCC()
                            .addingGID(), { res in
            switch res {
            case .success(_):
                completion(nil)
            case .failure(let err):
                completion(err)
            }
        })
    }
    
    /// Set data for the user for a specified key. This data is stored in application group scope and is available to all applications belonging to the same application group. If the given key already exists, it will be overwritten.
    /// - Parameters:
    ///   - userId: The user's ID.
    ///   - key: the scecified key
    ///   - data: the scecified key
    ///   - completion: closure called with the result
    public func setApplicationUserData(userId: String, key: String, data: String, _ completion: @escaping (Error?) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(method: .post)
                            .adding(payload: .json(["\(key)": "\(data)"]))
                            .appending(queryParameters: ["key": key])
                            .appending(headerParameters: ["Content-Type":"application/json; charset=utf-8"])
                            .adding(path: "group/\(userId)")
                            .addingCC()
                            .addingGID(), { res in
            switch res {
            case .success(_):
                completion(nil)
            case .failure(let err):
                completion(err)
            }
        })
    }
}
