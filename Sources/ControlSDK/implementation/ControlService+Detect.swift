//
//  ControlService+Detect.swift
//  
//
//  Created by Gabor Bottyan on 2022. 03. 03..
//

import Foundation

extension ControlService: Detect {
    public func applicationLogLevel(_ completion: @escaping (Result<String, Error>) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "log/level")
                            .addingCC()
                            .addingGID(), { res in
            
        })
    }
    
    public func log(message: ControlLogEntry, _ completion: @escaping (Error?) -> Void) {
        do {
            let data = try JSONEncoder().encode(message)
            executeWithSession(request: ControlRequest(configuration: configuration)
                                .adding(method: .post)
                                .adding(payload: .data(data))
                                .adding(path: "application/log")
                                .addingCC()
                                .addingGID(), { res in
                switch res {
                case .success(_):
                    completion(nil)
                case .failure(let e):
                    completion(e)
                }
                
            })
        } catch let e {
            completion(e)
        }
    }
    
    public func log(messages: [ControlLogEntry], _ completion: @escaping (Error?) -> Void) {
        do {
            let data = try JSONEncoder().encode(messages)
            executeWithSession(request: ControlRequest(configuration: configuration)
                                .adding(method: .post)
                                .adding(payload: .data(data))
                                .adding(path: "application/logs")
                                .addingCC()
                                .addingGID(), { res in
                switch res {
                case .success(_):
                    completion(nil)
                case .failure(let e):
                    completion(e)
                }
            })
        } catch let e {
            completion(e)
        }
    }
    
}
