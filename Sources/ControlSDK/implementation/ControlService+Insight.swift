//
//  ControlService+Insight.swift
//  
//
//  Created by Gabor Bottyan on 2022. 03. 03..
//

import Foundation

extension ControlService: Insight {
    
    public func applicationStart(_completion: @escaping (Error?) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(method: .post)
                            .adding(path: "event/log")
                            .adding(payload: .json(["eventType": "START"]))
                            .addingGID(), { res in
            
        })
    }
    
    public func applicationQuit(retentionTimne: Int, _completion: @escaping (Error?) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(method: .post)
                            .adding(path: "event/log")
                            .adding(payload: .json(["eventType": "QUIT", "retentionTime": retentionTimne]))
                            .addingGID(), { res in
            
        })
    }
}
