//
//  ControlService+Control.swift
//  
//
//  Created by Gabor Bottyan on 2/23/22.
//

import Foundation

extension ControlService: Control {
    
    /// Applications can be in either "Active" or "Maintenance" states. When the application is in a maintenance state, a message (editorial) is returned by this endpoint, which could be used to notify end-users about temporary disturbances in the service.
    /// - Parameter completion: closure called with the result
    public func applicationStatus(_ completion: @escaping (Result<ControlStatus, Error>) -> Void) {
        executeWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "status")
                            .addingCC()
                            .addingGID(), responseToTypeMapping(completion))
    }
    
    
    public func metadata(_ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "metadata")
                                    .addingCC()
                                    .addingGID(), responseToDataMapping(completion))
    }
    
    
    public func metadata(key: String, _ completion: @escaping (Result<Data, Error>) -> Void ) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "metadata/\(key)")
                                    .addingCC()
                                    .addingGID(), responseToDataMapping(completion))
    }
    
    
    public func metadata(keys: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "metadata/\(keys.joined(separator: ","))")
                                    .addingCC()
                                    .addingGID(), responseToDataMapping(completion))
    }
    
    
    public func assets(_ completion: @escaping (Result<[String: String], Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "asset")
                                    .addingCC()
                                    .addingGID(), responseToTypeMapping(completion))
    }
    
    
    public func getProfile(_ completion: @escaping (Result<ControlProfile, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "profile")
                                    .addingCC()
                                    .addingGID(), responseToTypeMapping(completion))
    }
}
