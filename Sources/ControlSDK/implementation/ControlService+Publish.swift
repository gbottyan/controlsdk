//
//  ControlService+Publish.swift
//  
//
//  Created by Gabor Bottyan on 2/23/22.
//

import Foundation

extension ControlService: Publish {
    
    public func allEntries(params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "content/entries")
                                    .adding(params: params), responseToTypeMapping(completion))
    }
    
    public func entryById(id: String, params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                            .adding(path: "content/entry/\(id)")
                            .appending(headerParameters: ["Accept": "application/json", "Content-Type": "application/json"])
                            .adding(params: params), responseToDataMapping(completion))
    }
    
    public func entryByAlias(alias: String, params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "content/entry/alias/\(alias)")
                                    .adding(params: params), responseToDataMapping(completion))
    }
    
    public func entriesByIds(ids: [String], params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "content/entries")
                                    .appending(queryParameters: ["id": ids.joined(separator: ",")])
                                    .adding(params: params), responseToDataMapping(completion))
    }
    
    public func entriesByAliases(aliases: [String], params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "content/entries")
                                    .appending(queryParameters: ["alias": aliases.joined(separator: ",")])
                                    .adding(params: params), responseToDataMapping(completion))
    }
    
    public func entriesByTypeId(typeIds: [String], params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "content/entries")
                                    .appending(queryParameters: ["typeId": typeIds.joined(separator: ",")])
                                    .adding(params: params), responseToDataMapping(completion))
    }
    
    
    public func entriesByTypeAlias(typeAlias: String, params: ControlParams?, _ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "content/entries")
                                    .appending(queryParameters: ["typeAlias": typeAlias])
                                    .adding(params: params), responseToDataMapping(completion))
    }
    
    public func allLocales(_ completion: @escaping (Result<Data, Error>) -> Void) {
        executeCachedWithSession(request: ControlRequest(configuration: configuration)
                                    .adding(path: "locales"), responseToDataMapping(completion))
    }
}
