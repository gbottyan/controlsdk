//
//  ControlService+PublishExt.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation
import ControlSDK
import Promises

extension ControlService: PublishExt {
    
    enum EntryKeys: String {
        case meta = "_meta"
        case entries
        case id
    }
    
    func entryById(id: String, params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        entryById(id: id, params: params, { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        })
    }
    
    func entryByAlias(alias: String, params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        entryByAlias(alias: alias, params: params, { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        })
    }
    
    func entriesByIds(ids: [String], params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        entriesByIds(ids: ids, params: params) { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        }
    }
    
    func entriesByAliases(aliases: [String], params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        entriesByAliases(aliases: aliases, params: params, { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        })
    }
    
    func entriesByTypeId(typeIds: [String], params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        entriesByTypeId(typeIds: typeIds, params: params) { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        }
    }
    
    func entriesByTypeAlias(typeAlias: String, params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        entriesByTypeAlias(typeAlias: typeAlias, params: params) { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        }
    }
    
    func allEntries(params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        allEntries(params: params) { res in
            switch res {
            case let .success(data):
                self.queryRecursive(data: data, recursive: recursive, completion)
            case let .failure(e):
                completion(.failure(e))
            }
        }
    }
    
    func createPromises(ids: [String]) -> [Promise<Data>] {
        var res: [Promise<Data>] = []
        let chunks = ids.chunked(into: 50)
        
        for chunk in chunks {
            res.append(Promise<Data> (on: .global(qos: .background), { (fullfill, reject) in
                self.entriesByIds(ids: chunk, params: nil, { res in
                    switch res {
                    case let .success(data):
                        fullfill(data)
                    case let .failure(e):
                        reject(e)
                    }
                })
            }))
        }
        return res
    }
    
    func queryRecursive(data: Data, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void) {
        if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            let ids = self.collectIds(dict: json, keys: recursive)
            if ids.isEmpty {
                completion(.success(data))
            } else {
                all(self.createPromises(ids: ids)).then { (results) in
                    do {
                        let breakDown = try self.consolidate(datas: results)
                        let res = try self.rebuild(dict: json, keys: recursive, data: breakDown)
                        let newdata = try JSONSerialization.data(withJSONObject: res, options: [.prettyPrinted])
                        self.queryRecursive(data: newdata, recursive: recursive, completion)
                    } catch let e {
                        completion(.failure(e))
                    }
                }.catch { e in
                    completion(.failure(e))
                }
            }
        } else {
            completion(.failure(ControlError.decodeError))
        }
    }
    
    func consolidate(datas: [Data]) throws -> [String: Any] {
        var res:[String: Any] = [:]
        for data in datas {
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                if let entries = json[EntryKeys.entries.rawValue] as? [Any] {
                    for e in entries {
                        if let entry = e as? [String: Any] {
                            if let meta = entry[EntryKeys.meta.rawValue] as? [String: Any], let id = meta[EntryKeys.id.rawValue] as? String {
                                res[id] = entry
                            } else {
                                throw ControlError.idNotFound
                            }
                        } else {
                            throw ControlError.entryNotaDictionary
                        }
                    }
                } else {
                    throw ControlError.entriesNotFound
                }
            } else {
                throw ControlError.decodeError
            }
        }
        return res
    }
    
    func collectIds(dict: [String: Any], keys: [String]) -> [String] {
        var res: [String] = []
        for (k, v) in dict {
            if keys.contains(k) {
                if let entryId = v as? String {
                    res.append(entryId)
                } else if let entryIds = v as? [String], !entryIds.isEmpty {
                    res.append(contentsOf: entryIds)
                }
            }
            if let arr = v as? [Any], !arr.isEmpty {
                for e in arr {
                    if let childDict = e as? [String: Any] {
                        res.append(contentsOf: collectIds(dict: childDict, keys: keys))
                    }
                }
            } else if let childDict =  v as? [String: Any] {
                res.append(contentsOf: collectIds(dict: childDict, keys: keys))
            }
        }
        return res
    }
    
    func rebuild(dict: [String: Any], keys: [String], data: [String: Any]) throws -> [String: Any] {
        var res: [String: Any] = dict
        
        for (k, v) in dict {
            if let arr = v as? [Any] {
                var array: [Any] = []
                for e in arr {
                    if let childDict = e as? [String: Any] {
                        array.append(try rebuild(dict: childDict, keys: keys ,data: data))
                    } else {
                        array.append(e)
                    }
                }
                res[k] = array
            } else if let childDict =  v as? [String: Any] {
                res[k] = try rebuild(dict: childDict, keys: keys ,data: data)
            }
            
            if keys.contains(k) {
                if let entryId = v as? String {
                    if let resolved = data[entryId] {
                        res[k] = resolved
                    } else {
                        throw ControlError.entriesNotFound
                    }
                    
                } else if let entryIds = v as? [String] {
                    var array: [Any] = []
                    for entryId in entryIds {
                        if let resolved = data[entryId] {
                            array.append(resolved)
                        } else {
                            throw ControlError.entriesNotFound
                        }
                    }
                    res[k] = array
                }
            }
        }
        return res
    }
}

