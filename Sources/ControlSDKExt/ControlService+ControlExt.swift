//
//  ControlService+PublishExt.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation
import ControlSDK
import Promises

extension ControlService: ControlExt {
    public func assets(keys: [String], _ completion: @escaping (Result<[String : Data], Error>) -> Void) {
        assets { res in
            switch res {
            case let .success(dict):
                let allKeys = dict.keys.filter { item in
                    keys.contains(item)
                }
                if allKeys.isEmpty {
                    completion(.failure(ControlError.idNotFound))
                } else {
                    all(self.createAssetPromises(keys: allKeys, dict: dict)).then { (results) in
                        var res: [String: Data] = [:]
                        for (i,k) in allKeys.enumerated() {
                            res[k] = results[i]
                        }
                        completion(.success(res))
                        
                    }.catch { e in
                        completion(.failure(e))
                    }
                }
                
            case let .failure(e):
                completion(.failure(e))
            }
        }
    }
    
    func createAssetPromises(keys: [String], dict: [String: String]) -> [Promise<Data>] {
        var res: [Promise<Data>] = []
        for key in keys {
            if let url = dict[key] {
                res.append(Promise<Data> (on: .global(qos: .background), { (fullfill, reject) in
                    self.executeCachedWithSession(request: ControlRequest(configuration: self.configuration, url: URL(string: url)!), { res in
                        switch res {
                        case let .success(data):
                            if let data = data.data {
                                fullfill(data)
                            } else {
                                reject(ControlError.entriesNotFound)
                            }
                        case let .failure(e):
                            reject(e)
                        }
                    })
                }))
            }
        }
        return res
    }
}
