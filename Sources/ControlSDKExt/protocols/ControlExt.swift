//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation
import ControlSDK
protocol ControlExt: Control {
    func assets(keys: [String], _ completion: @escaping (Result<[String: Data], Error>) -> Void)
}
