//
//  File.swift
//  
//
//  Created by Gabor Bottyan on 2022. 02. 28..
//

import Foundation

import ControlSDK
protocol PublishExt: Publish {
    func entryById(id: String, params: ControlParams?, recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    func entryByAlias(alias: String, params: ControlParams?,recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    func entriesByIds(ids: [String], params: ControlParams?,recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    func entriesByAliases(aliases: [String], params: ControlParams?,recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    func entriesByTypeId(typeIds: [String], params: ControlParams?, recursive: [String],_ completion: @escaping (Result<Data, Error>) -> Void)
    func entriesByTypeAlias(typeAlias: String, params: ControlParams?,recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    func allEntries(params: ControlParams?,recursive: [String], _ completion: @escaping (Result<Data, Error>) -> Void)
    func allLocales(_ completion: @escaping (Result<Data, Error>) -> Void)
}
